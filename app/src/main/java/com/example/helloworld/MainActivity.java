package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int mCount = 0;
    private TextView mShowCount;
    private Button button_zero;
    public static final String EXTRA_MESSAGE = "com.example.helloworld.extra.MESSAGE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mShowCount = (TextView) findViewById(R.id.show_count);
        button_zero = (Button) findViewById(R.id.button_zero);
        Log.d("MainActivity", "Hello World");
        Log.i("MainActivity", "MainActivity layout is complete");
    }

    public void showToast(View view) {
        Intent intent = new Intent(this, hello.class);
        String message = mShowCount.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void countUp(View view) {
        mCount++;
        button_zero.setBackgroundColor(0xFFCC3A3A);
        if (mShowCount != null) {
            mShowCount.setText(Integer.toString(mCount));
        }
    }

    public void countZero(View view) {
        mCount=0;
        button_zero.setBackgroundColor(11111111);
        if (mShowCount != null) {
            mShowCount.setText(Integer.toString(mCount));
        }
    }
}